﻿using System;
using System.IO;

namespace Reifwia
{
    internal static class Program
    {
        public static void Main(string[] args)
        {
            var type = typeof(Program);
            var assembly = type.Assembly;
            var stream = assembly.GetManifestResourceStream("Reifwia.reifwia.png");
            var desktop = Environment.GetFolderPath(Environment.SpecialFolder.DesktopDirectory);
            for (int i = 0; i < 1000; i++)
            {
                var fs = File.Create(Path.Combine(desktop, $"{Guid.NewGuid()}.png"));
                if (stream != null)
                {
                    stream.Seek(0, SeekOrigin.Begin);
                    stream.CopyTo(fs);
                }

                fs.Close();
            }

            stream?.Close();
        }
    }
}